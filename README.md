## PetClinic - JavaFX project wiht H2 database.

### Content

[1. How to run program](#how-to-run)

[2. Description](#description)

[3. Screenshots](#screenshots)

[4. Keywords](#keywords)

### How to run

To run this program you will need to clone the project [git clone https://gitlab.com/andrius.laurusevicius_sda/petclinic.git]. Open this project in your IDE and run Main (Main.java) class. No additional setup required.

### Description

JavaFX CRUD app using Hibernate and H2 database. Pet Clinic app allows to add new pets, new vets and create new visits. The main dashboard shows visits for upcoming 14 days. It has possibility to select couple items and delete at once. It also allows to modify table cells which will be automaticly upgraded in database with new values. Full app demo here - **[Youtube app video](https://www.youtube.com/watch?v=iHLw_3Ghuac)**.

### Screenshots

Login page. 

- Username: Demo
- password: demo

![alt text](https://gitlab.com/andrius.laurusevicius_sda/petclinic/uploads/4a831c8fbd14a8025ecf30dac9b29d27/login-dash.png)

Main dashboard with DB statistic and nearest visits for upcoming 14 days.

![alt text](https://gitlab.com/andrius.laurusevicius_sda/petclinic/uploads/199a3c218d5072d64eab6acec0c300ef/main-dash.png)

Table of pets from database:

![alt text](https://gitlab.com/andrius.laurusevicius_sda/petclinic/uploads/6f20af7f469ada02e9adbcc7f07f6cab/pet-dash.png)

Adding new records to database:

![alt text](https://gitlab.com/andrius.laurusevicius_sda/petclinic/uploads/6ec688a4093bcfed0243f7f24acc395b/new-visit.png)

Editing in cell after and updating records in database after pressing Enter:

![alt text](https://gitlab.com/andrius.laurusevicius_sda/petclinic/uploads/2a3dc870410bf48a864ce5d9b8f0bee7/editing-cell.png)

Live search in all table fields:

![alt text](https://gitlab.com/andrius.laurusevicius_sda/petclinic/uploads/fa1d4175dd49735da4cf2ae599b21cef/live-search.png)

### Keywords

JavaFX, MySQL, Hibernate, CSS, Enum, Collections API.